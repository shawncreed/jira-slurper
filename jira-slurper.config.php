<?php

/*
 * JIRA Slurper example configuration
 */

if(file_exists("jira-slurper.config-local.php")) {
	require_once("jira-slurper.config-local.php");
	return;
}

// Enable debug messages?
define('DEBUG', 'false');

// JIRA credentials
define('JIRA_USERNAME', 'johnsmith@merkleinc.com');
define('JIRA_PASSWORD', 'yourpassword');

// Base URL of your JIRA installation
define('JIRA_URL', 'https://5thinternational.jira.com');

// The default reporter value to use (probably your own username)
define('JIRA_DEFAULT_REPORTER', 'johnsmith');

// The IDs for your issue types (probably don't need to change this)
$issuetype_list = array(
		"bug" => 1,
		"epic" => 5,
		"story" => 6,
		"task" => 3,
	);

// The IDs for your priorities (probably don't need to change this)
$priority_list = array(
		"critical" => 2,
		"major" => 3,
		"minor" => 4,
		"trivial" => 5,
		"high" => 6,
		"low" => 7,
		"normal" => 8,
	);

// The IDs for your components (probably don't need to change this)
$components_list = array(
	);
