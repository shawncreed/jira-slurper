#!/usr/bin/php
<?php
/**
 * JIRA slurper
 * @version 0.1
 * @author Shawn Reed <shawn.c.reed@gmail.com>
 */

// Load configuration
require_once("jira-slurper.config.php");

// Build authentication string
define('BASIC_AUTH_STRING', base64_encode(JIRA_USERNAME . ":" . JIRA_PASSWORD));

// Initialize handler and set up HTTP headers
$ch = curl_init();
$http_headers = array(
		"Authorization: Basic " . BASIC_AUTH_STRING,
		"Content-Type: application/json",
	);
curl_setopt($ch, CURLOPT_URL, JIRA_URL . "/rest/api/2/issue");
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, $http_headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
if(defined('DEBUG') && DEBUG == 'true') {
	curl_setopt($ch, CURLOPT_VERBOSE, true);
}

// Validate file was specified and is valid
if(!isset($argv) || $argv[1] == "") {
	die(" Usage: slurp <filename>\n");
}
if(!file_exists($argv[1])) {
	die(" Error: slurp file doesn't appear to exist: $argv[1]\n");
}
$file = file($argv[1]);
if(count($file) < 1) {
	die(" Error: slurp file appears to be empty: $argv[1]\n");
}

// Parse input file and build POST data object
$last_epic = "";
$errors = array();
$data = new stdClass();
initialize_fields();
foreach($file as $line) {
	debug(trim($line));
	if(substr($line, 0, 1) == "#") { continue; } // ignore comments
	list($key, $value) = explode("->", $line);
	$key = trim($key);
	$value = trim($value);
	if($value == "" && trim($line) != "--") {
		if($last_key == "description") {
			$fields->description = $fields->description . "\n" . trim($key);
		}
		continue; // ignore invalid lines for now
	}
	if(trim($line) == "--") {
		$last_key = "";
		create_issue();
		initialize_fields();
		continue;
	}
	switch($key) {
		case 'epic link':
			if($value == "*") {
				if($last_epic != "") {
					echo "*** INFO: Linking issue to epic $last_epic\n";
					$fields->customfield_11100 = $last_epic;
				}
			} else {
				$fields->customfield_11100 = $value;
			}
			break;
		case 'issuetype':
		case 'priority':
		case 'components':
			$fields->$key = new stdClass();
			$index = strtolower($value);
			$list_name = $key . "_list";
			$fields->$key->id = (string) ${$list_name}[$index];
			if($fields->$key->id == "") {
				$errors[] = "*** WARNING: Invalid $key specified";
			}
			break;
		case 'labels':
			$labels = explode(",", trim($value));
			$fields->labels = $labels;
			break;
		case 'project':
			$project = new stdClass();
			if(is_numeric($value)) {
				$project->id = (string) $value;
			} else {
				$project->key = $value;
			}
			$fields->project = $project;
			break;
		case 'assignee':
		case 'reporter':
			$fields->$key = new stdClass();
			$fields->$key->name = $value;
			break;
		default:
			$fields->$key = $value;
			break;
	}
	$last_key = $key;
}
if(isset($fields->summary) && isset($fields->description)) {
	create_issue();	
}

// Initialize fields object for a new issue
function initialize_fields() {
	debug("Initializing fields");
	global $fields, $priority_list, $project;
	$fields = new stdClass();
	$fields->reporter = new stdClass();
	$fields->reporter->name = JIRA_DEFAULT_REPORTER;
	$fields->priority = new stdClass();
	$fields->priority->id = (string) $priority_list["normal"];
	$fields->project = $project;
	return true;
}

/**
 * Send data object to JIRA if valid
 * @return boolean Indicates whether or not issue was successfully created
 */
function create_issue() {
	debug("Creating issue");
	global $ch, $data, $fields, $errors, $project, $issuetype_list, $last_epic;
	if(!isset($fields->summary) || $fields->summary == "") {
		$errors[] = "** WARNING: Missing summary";
	}
	if(!isset($project->id)) {
		$errors[] = "** WARNING: Missing project ID";
	}
	$issuetype_index = $fields->issuetype->id;
	$issuetype = strtolower(array_search($issuetype_index, $issuetype_list));
	debug("Issuetype_index = $issuetype_index");
	debug("Issuetype = $issuetype");
	if($issuetype == "epic") {
		$fields->customfield_11101 = $fields->summary;
	}
	$data->fields = $fields;
	if(count($errors) > 0) {
		foreach($errors as $error) {
			echo $error . "\n";			
		}
		echo "** ERROR: The following issue could not be created\n";
		echo "-- Start affected issue --\n";
		print_r($data);
		echo "-- End affected issue --\n\n";
		$errors = array();		
		return false;
	} else {
		$errors = array();
		echo "*** INFO: Sending data as new issue request\n";
		$json = json_encode($data);
		$ajson = json_decode($json, true);
		$json = json_encode($ajson);
		echo $json;
		echo "\n";
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		$result = curl_exec($ch);
		$result = json_decode($result);
		if(isset($result->key)) {
			if($issuetype == "epic") {
				$last_epic = $result->key;
			}
			echo "*** INFO: Success! Created new {$issuetype} as {$result->key}: {$fields->summary}\n\n";
			return true;
		} else {
			echo "*** ERROR: Couldn't create issue due to an error returned from the JIRA API\n";
			print_r($result);
			echo "\n";
			return false;
		}
	}
}

/**
 * Output a debug message to stdout
 * @return boolean Always returns true
 */
function debug($msg) {
	if(!defined('DEBUG') || DEBUG != 'true') { return false; }
	echo "*** DEBUG: $msg\n";
	return true;
}

// Clean up and exit
curl_close($ch);
exit;