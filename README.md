# jira-slurper

A script to import new issues into JIRA in batch from a plain text file.

## Installation

It's recommended that you do the following via sudo to ensure you will have the needed permissions.

* Download this repository and move it somewhere safe like /usr/local/jira-slurper, then do the following commands
* `cd /usr/local/jira-slurper` *(assuming that is the path you selected on the previous step)*
* `chmod a+x slurp.php`
* `ln -s slurp.php /usr/local/bin/slurp` *(make sure this directory is in your $PATH)*

## Configuration

See the file `jira-slurper-config.php` included with this repository.

## Usage

`slurp <filename>`

## Input file format

See the file `example.slurp` included with this repository.

## Questions?

E-mail me at shawn.c.reed@gmail.com.
